import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
// import './App.css';

const RSSCard = (title) => {

  return (
    <div>
      <h1>{title.title}</h1>
      <p dangerouslySetInnerHTML={{__html: title.waitTime, sanitize: true}}></p>
    </div>
  );
}

const RSSFeed = () => {
  const RSS_URL = `https://bwt.cbp.gov/api/bwtRss/HTML/-1/57,55/-1`;
  const [feed, setFeed] = useState([]);
  const [counter, setCounter] = useState(0);
  const callRSS = () => {
    const request = new XMLHttpRequest();
    const convert = require('xml-js');
    request.onreadystatechange = () => {
        if (request.readyState == 4 && request.status == 200) {
          const json = JSON.parse(convert.xml2json(request.responseText, { compact: true, spaces: 4 }));
          setFeed(json.rss.channel.item);
      }
    }

    request.open("GET", RSS_URL, true);
    request.send();
  }
  useEffect(() => {
    const interval = setInterval(() => {
      setCounter(counter + 1);
      callRSS()
    }, 1000);
    return () => clearInterval(interval);
  }, [feed, counter]);

  return (
    <div>
      {feed.map((text) => <RSSCard key={text.title._text} title={text.title._text} waitTime={text.description._text.join('<br/>')} /> )}
      <h3>{counter}</h3>
    </div>
  );
}

const App = () => {
  return (
    <div className="App">
      <RSSFeed />
    </div>
  );
}

export default App;
